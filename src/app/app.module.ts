import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ComorbiditeComponent } from 'src/comorbidite/comorbidite.component';
import { HttpClientModule } from '@angular/common/http';
import { ComorbiditeFormComponent } from 'src/comorbidite-form/comorbidite-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
const appRoutes: Routes = [
  { path: 'comorbidites/form', component: ComorbiditeFormComponent },
  { path: 'comorbidites', component: ComorbiditeComponent },
  { path: '',
    redirectTo: '/comorbidites',
    pathMatch: 'full'
  },
//  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
   declarations: [
      AppComponent,
      ComorbiditeComponent,
      ComorbiditeFormComponent
   ],
   imports: [
      BrowserModule,
      HttpClientModule,
      ReactiveFormsModule,
      RouterModule.forRoot(
        appRoutes,
        { enableTracing: true } // <-- debugging purposes only
      )
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
