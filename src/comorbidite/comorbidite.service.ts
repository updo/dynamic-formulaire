import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Comorbidite } from './comorbidite';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComorbiditeService {
  api = '/api/comorbidites';

  constructor(private http: HttpClient) {}

  public findAll(): Observable<Comorbidite[]> {
    return this.http.get<Comorbidite[]>(this.api);
  }

  public get(id: number): Observable<Comorbidite> {
    return this.http.get<Comorbidite>(`${this.api}/${id}`);
  }
  public create(comorbidite: Comorbidite): Observable<Comorbidite> {
    return this.http.post<Comorbidite>(this.api, comorbidite);
  }
  public update(id: number, comorbidite: Comorbidite): Observable<Comorbidite> {
    return this.http.put<Comorbidite>(`${this.api}/${id}`, comorbidite);
  }
}
