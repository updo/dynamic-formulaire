import { Categorie } from './categorie';

export class Parametres {
  id: number;
  label: string;
  inputType: string;
  name: string;
  categorie: Categorie;
}
