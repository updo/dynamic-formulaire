import { Component, OnInit } from '@angular/core';
import { ComorbiditeService } from './comorbidite.service';
import { Comorbidite } from './comorbidite';

@Component({
  selector: 'app-comorbidite',
  templateUrl: './comorbidite.component.html',
  styleUrls: ['./comorbidite.component.css']
})
export class ComorbiditeComponent implements OnInit {
  comorbidites: Comorbidite[];

  constructor(private service: ComorbiditeService) { }

  ngOnInit() {
     this.service.findAll()
     .subscribe(datas => this.comorbidites = datas);
  }

}
