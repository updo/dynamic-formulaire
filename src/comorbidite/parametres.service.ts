import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Parametres } from './parametres';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ParametresService {
  api = '/api/comorbidites/parametres';

  constructor(private http: HttpClient) { }

  public findAll(): Observable<Parametres[]> {
    return this.http.get<Parametres[]>(this.api);
  }

  public get(id: number): Observable<Parametres> {
    return this.http.get<Parametres>(`${this.api}/${id}`);
  }
  public create(parametre: Parametres): Observable<Parametres> {
    return this.http.post<Parametres>(this.api, parametre);
  }
  public update(id: number, parametre: Parametres): Observable<Parametres> {
    return this.http.put<Parametres>(`${this.api}/${id}`, parametre);
  }
}
