import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Categorie } from './categorie';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategorieService {

  api = '/api/comorbidites/categories';

  constructor(private http: HttpClient) {}

  public findAll(): Observable<Categorie[]> {
    return this.http.get<Categorie[]>(this.api);
  }

  public get(id: number): Observable<Categorie> {
    return this.http.get<Categorie>(`${this.api}/${id}`);
  }
  public create(categorie: Categorie): Observable<Categorie> {
    return this.http.post<Categorie>(this.api, categorie);
  }
  public update(id: number, categorie: Categorie): Observable<Categorie> {
    return this.http.put<Categorie>(`${this.api}/${id}`, categorie);
  }

}
