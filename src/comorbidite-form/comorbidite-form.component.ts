import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { Comorbidite } from 'src/comorbidite/comorbidite';
import { Categorie } from 'src/comorbidite/categorie';
import { CategorieService } from 'src/comorbidite/categorie.service';
import { ComorbiditeService } from 'src/comorbidite/comorbidite.service';
import { Router } from '@angular/router';
import { ParametresService } from 'src/comorbidite/parametres.service';
import { Parametres } from 'src/comorbidite/parametres';

@Component({
  selector: 'app-comorbidite-form',
  templateUrl: './comorbidite-form.component.html',
  styleUrls: ['./comorbidite-form.component.css']
})
export class ComorbiditeFormComponent implements OnInit {

  form: FormGroup;
  @Input() comorbidite: Comorbidite ;
  categories: Categorie[];
  parametres: Parametres[];

  constructor(private categorieService: CategorieService,
              private comorbiditeService: ComorbiditeService,
              private parametreService: ParametresService,
              private router: Router) { }

  ngOnInit() {
    this.categorieService
        .findAll()
        .subscribe(datas =>  {
          this.categories =  datas;
          this.loadParametres();

        });
    this.form =  new FormGroup({
       id:  new FormControl(''),
       comorbidites: new FormGroup({})
    });
  }

  private loadParametres() {
    this.parametreService
      .findAll()
      .subscribe(datas => {
        this.parametres = datas;
        this.parametres.forEach(parametre => (this.form.get('comorbidites') as FormGroup).addControl(parametre.name, new FormControl('')));
      });
  }

    filterParameterByCategory(categorie: Categorie): Parametres[]{
      if(!categorie || !this.parametres){
        return ;
      }
      return this.parametres.filter(parametre => parametre.categorie && parametre.categorie.id === categorie.id);
    }

  submitForm() {
    const comorbiditeNew =  this.form.value;
    this.comorbiditeService.create(comorbiditeNew).subscribe(data => {
      this.router.navigate(['/comorbidites']);
    });
  }


}
